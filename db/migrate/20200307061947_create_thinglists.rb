class CreateThinglists < ActiveRecord::Migration[6.0]
  def change
    create_table :thinglists do |t|
      t.String :thingid
      t.String :thingprice
      t.String :thingquantity

      t.timestamps
    end
  end
end
